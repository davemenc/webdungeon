from icecream import ic
import json
import os
import time
from character import Character
from item import Item
from door import Door
from room import Room
from weapon import Weapon
from magic import Magic
from armor import Armor
from container import Container

# IC Settings
ic.enable()

class MyMap:
	def backupname(fname):
		name = fname[:-4] # remove extension
		# add a 6 digit number that's roughly seconds from beginning of year
		START = 1700348266.206527
		val = int(time.time()-START)
		name += '{:0>6}'.format(val)
		return "bk/"+name+".bak"

	def load_map(fname):
		if os.path.isfile(fname):
			with open(fname,"rt") as f:
				data = json.load(f)
			m = MyMap.ingest(data)
			return m
		else:
			print(f"{fname} does not exist")
			exit()

	def save_map(self,fname,append=False):
		opentype = "wt"
		if append:
			opentype="at"
		if os.path.isfile(fname):
			os.rename(fname, MyMap.backupname(fname))
		map_dict = self.export()
		data = json.dumps(map_dict,indent=4)
		with open(fname,opentype) as f:
			f.write(data)

	def ingest(map_dict):
		m = MyMap(map_dict['id'],map_dict['name'],map_dict['desc'],map_dict['first_text'],map_dict['first_room'])
		for r in map_dict['rooms']:
			newroom = Room.ingest(r)
			m.add_room(newroom)
		return m

	def __init__(self,id,name,description,first_text,first_room,fname=None):
		self.id = id
		self.desc = description
		self.name = name
		self.first_text = first_text
		self.fname = fname
		self.first_room=first_room
		if self.fname is not None:
			self.read_file(fname)
		self.rooms={}
		self.player = None
	
	def __repr__(self):
		return f"Map {self.name} ({self.id}): {self.desc}; Starts in room {self.first_room}...\n\tFirst Text: {self.first_text}\n\tRooms: {self.rooms}"

	def export(self):
		#characters = []
		#for k,char in self.characters.items():
		#	characters.append(char.export())
		rooms = []
		for k,room in self.rooms.items():
			rooms.append(room.export())
		#doors = []
		#for k,door in self.doors.items():
		#	doors.append(door.export())
		#containers = []
		#for k,container in self.containers.items():
		#	containers.append(container.export())
		topmap = {"id":self.id,"desc":self.desc,"name":self.name,"first_text":self.first_text,"first_room":self.first_room,'rooms':rooms}
		return topmap

	def add_room(self,room):
		if room.id in self.rooms:
			print("DUPLICATE ROOM ID",room,"and",self.rooms[room.id])
			return False
		else:
			self.rooms[room.id]=room
			return True

	def get_room(self,roomid):
		return self.rooms[roomid]
		
	def add_door(self,door):
		if door.id in self.doors:
			print("DUPLICATE DOOR ID",door,"and",self.doors[door.id])
			return False
		else:
			self.doors[door.id]=door

	def add_item(self,item):
		if item.id in self.items:
			print("Duplicate item",item,"and",self.item[item.id])
		else:
			self.items[item.id]=item

	def add_container(self,con):
		if con.id in self.containers:
			print("Duplicate con",con,"and",self.con[con.id])
		else:
			self.containers[con.id]=con
	def add_character(self,char):
		if char.id in self.characters:
			print("Duplicate char",char,"and",self.char[char.id])
		else:
			self.characters[char.id]=char
	def add_player(self,player):
		self.player=player

if __name__ == "__main__":
	ic ("MyMap code compiles")
	mymap = MyMap.load_map("Four-foldRiddle.json")
	print(mymap)
	# mymap.save_map("dungeon2.json")

	print("\nFINISHED WITHOUT ERROR\n")

