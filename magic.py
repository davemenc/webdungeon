from icecream import ic 
from item import Item
import json

class Magic(Item):
	MAGIC_TYPES = ["Healing","Seeing","TrapDetect","IronSkin","Attack","Power","Accuracy"]

	def __init__(self,id,name,desc,value,mtype):
		self.mtype = mtype
		super().__init__(id,name,"Magic",value,desc)

	def __repr__(self):
		return f"{self.name}({self.id}): {self.type},{self.desc}, {self.mtype}"

	def export(self):
		return {"name":self.name,"id":self.id,"type":self.type,"desc":self.desc,"mtype":self.mtype,"value":self.value}

if __name__ == "__main__":
	print ("Magic code compiles")
	m = Magic("M1","Potion Of Seeing","A clear potion that shimmers.",300,"Seeing")
	ic(m)

	exp=m.export()
	ic(exp)
	ic(json.dumps(exp))
	print("\nFINISHED WITHOUT ERROR\n")
