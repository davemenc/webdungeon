from icecream import ic
import json

class Item:
	ITEM_TYPES=["Weapon","Treasure","Armor","Key","Magic","Map"]

	def __init__(self,id,name,type,desc,value):
		self.id = id
		self.type = type
		self.desc = desc
		self.name = name
		self.value = value


	def __repr__(self):
		return f"{self.name}({self.id}): {self.type}, value: {self.value}; {self.desc} "
	def export(self):
		return {"name":self.name,"id":self.name,"type":self.type,"desc":self.desc,"value":self.value}
if __name__ == "__main__":
	print ("Item code compiles")
	i = Item("I3","Blue Gem","Treasure",1000,"A gem worth 1000 GP.")
	ic(i)
	exp=i.export()
	ic(exp)
	ic(json.dumps(exp))
	print("\nFINISHED WITHOUT ERROR\n")
