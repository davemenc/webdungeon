from icecream import ic 
import json
from item import Item

class Weapon(Item):
	def ingest(weapon_dict):
		newweapon = Weapon(weapon_dict['id'],weapon_dict['name'],weapon_dict['desc'],weapon_dict['value'],weapon_dict['hit'],weapon_dict['damage'])
		return newweapon

	def __init__(self,id,name,desc,value,hit,damage):
		self.hit = hit
		self.damage = damage
		super().__init__(id,name,"Weapon",desc,value)

	def __repr__(self):
		return f"{self.name}({self.id}): {self.type},{self.desc}, {self.hit}/{self.damage}"
		
	def export(self):
		return {"name":self.name,"id":self.name,"type":self.type,"desc":self.desc,"hit":self.hit,"damage":self.damage,"value":self.value}

if __name__ == "__main__":
	print ("weapon code compiles")
	w = Weapon("I5","Obsidian Blade of Orathos","A deadly sword forged from pitch-black obsidian, rumored to be able to slice through solid stone.",200,5,5)
	ic(w)
	exp=w.export()
	test_ing = Weapon.ingest(exp)
	ic(test_ing)


	#print(json.dumps(exp))
	print("\nFINISHED WITHOUT ERROR\n")
