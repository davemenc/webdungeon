from icecream import ic
import json 
from character import Character
from item import Item
from door import Door
from mymap import MyMap
from room import Room
from magic import Magic
from armor import Armor
from weapon import Weapon
from container import Container
from player import Player,generate_player

first_impression ="You come down a spiral stair way to the very apex of a triangular room. In the center of the room is the ugliest dwarf you've ever seen. At the far wall you can see a large chest. There are doors in the exact center of both other corners."
first_room = "R1"
player = generate_player()
map = MyMap(0,"Four-fold Riddle","This is a simple 3 room dungeon that explores the basics.",first_impression,first_room)
# ROOMS
room1 = Room("R1","Northern Triangle", "This is a triangular room. In the center of the room is the ugliest dwarf you've ever seen. At the far wall you can see a large chest. There are doors in the exact center of both other corners.")
room2 = Room("R2","Western Triangle", "This triangular room is guarded by a soldier in a formal uniform. You hope he is more used to parading than fighting. There is a cupboard on the opposite wall. There is a door in the SE corner which is called the 'East Door'")
room3 = Room("R3","Eastern Triangle", "You find yourself in a Triangular room. There appears to be a tree growing out of the center of the room. You can see a simple box against the souther wall." )
room4 = Room("R4","Hidden Triangle", "There is a secret room here, tucked in between the East and West Triangles! It has a coffer in the exact center. ")
map.add_room(room1)
map.add_room(room2)
map.add_room(room3)
map.add_room(room4)

# Doors
#	def Door(id,name,target,type,description="Door"):

door2 =  Door("D2","West Passage","R1","Normal","A small door; you'll have to stoop to get through.")
door2a = Door("D2a","North Exit","R2","Normal","A small door; you'll have to stoop to get through.")
door3 =  Door("D3", "East Gateway","R1","Normal","An 8 foot door with a heavy iron lintel.")
door3a = Door("D3a","North Egress","R3","Normal","An 8 foot door with a heavy iron lintel.")
door4 =  Door("D4", "East Vestibule","R2","Normal","An ordinary looking door made of decent wood. Nothing to write home about.")
door4a = Door("D4a","West Threshold","R3","Normal","An ordinary looking door made of decent wood. Nothing to write home about.")
door5 =  Door("D5", "Secret Portal","R3","Secret","All shimmery and barely visible...")
door5a = Door("D5a","Egress",       "R4","Secret","All shimmery and barely visible...")
door6 =  Door("D6","Missing Door", "R2","Fake","It looks to be an incredibly stong door. It won't budge but there is a keyhole.")
door6a = Door("D6a","Invisible Door",   "R5","Fake","It looks to be an incredibly stong door. It won't budge but there is a keyhole.")

# Items

item1 = Magic("I1","Potion Of Seeing","A clear potion that shimmers.",300,"Seeing")

item2 = Armor("I2","Rough Hide Shield","",20,1,"Shield")
item3 = Item("I3","Red Gem","Treasure","A beautiful red gem.",100)
item4 = Item("I4","Golden Bracelet","Treasure","An intricately wrought gold bracelet.",100)
item5 = Weapon("I5","Obsidian Blade of Orathos","A deadly sword forged from pitch-black obsidian, rumored to be able to slice through solid stone.",200,5,5)
item6 = Item("I6","Necklace of the Siren Queen","Treasure","a delicate chain of sparkling aquamarine beads and pearls, once worn by a legendary mermaid ruler.",200)
item7 = Item("I7","A Golden Key","Key","A well wrought golden key",1)
item8 = Item("I8","Turquoise Bracelet of Xibalba","Treasure","wristlet of polished turquoise stones inscribed with ancient symbols from the lost civilization of the Maya.","magic_type")
item9 = Armor("I9","Dragon-scale Helmet","made of tough, scaly hide from a dragon, providing superior protection against blows.",5,100,"Helmet")
item10= Item("I10","The Holy Grail","Treasure","A cup that, according to legend, give eternal life.",1000)
item11= Item("I11","Golden Grimoire","Treasure","A tome of ancient and powerful spells bound in shimmering gold.",600)
item12= Item("I12","Flame-Red Ruby of Nagaroth","Treasure","desc",500)



# Containers
container1=Container("C1", "Gilded Coffer", "A wooden box with tiny threads of bright metal running every which way.","Simple",[item4,item5,item6])
container2=Container("C2", "Ornate Cache ", "A hole in the floor decorated with colorful red ants.","Locked",[item1,item2,item3])
container3=Container("C3", "Encrusted Ark ", "A gold box with detailed scenes of long ago and far away. Too bad it's too heavy to lift.","Breakable",[item7,item8,item9])
container4=Container("C4", "Lacquered Hoard ", "A strong box made of beautiful lacquered wood.","Simple",[item10,item11,item12])

# Monsters
m1 = Character("Ogre",5,5,5,5,"M1")
m2 = Character("Brigand",5,5,5,5,"M2")
m3 = Character("Pirate",5,5,5,5,"M3")
m4 = Character("Blue Meanie",5,5,5,5,"M4")


#container1.add_item(item4)
#container1.add_item(item5)
#container1.add_item(item6)

room1.add_character(m1)
room1.add_container(container1)
room1.add_door(door2)
room1.add_door(door3)



#container2.add_item(item1)
#container2.add_item(item3)
#container3.add_item(item2)
room2.add_character(m2)
room2.add_container(container2)
room2.add_door(door2a)
room2.add_door(door6)
room2.add_door(door4)

#container3.add_item(item7)
#container3.add_item(item8)
#container3.add_item(item9)
room3.add_character(m3)
room3.add_door(door4)
room3.add_door(door3a)
room3.add_door(door5)
room3.add_container(container3)



#container4.add_item(item10)
#container4.add_item(item11)
#container4.add_item(item12)
room4.add_character(m4)
room4.add_container(container4)

room4.add_door(door5a)

MAP_FNAME = "dungeon1.json"
print(f"Saving dungeon {map.name} to file '{MAP_FNAME}'.")
#map_dict = map.export()
#json_map = json.dumps(map_dict, indent=4)
#with open(MAP_FNAME,"wt") as f:
#	f.write(json_map)
map.save_map(MAP_FNAME)
print(f"Saved without error.")
print("\nFINISHED WITHOUT ERROR\n")

"""
D0 is first door but it's actually a "virtual door"
R1 is the first room
	d2
	d3
	C1 
		I4
		W5
		I6
	M1
R2
	d2
	d6
	d4


	C2
		M1
		I3
		A2
	M2
R3
	d4
	d3
	d5
	C3
		I7
		I8
		A9
	M3

R4
	d4
	d3
	d5
	C4
		I10
		I11
		I12
	M4

"""

