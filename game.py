from room import Room
from mymap import MyMap
from player import Player
from item import Item
from character import Character
from door import Door
from container import Container
from icecream import ic
import datetime

def logme(s):
	td = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
	with open("dungeon.log","at") as f:
		f.write(f"{td}: {s}\n")

class Game:
	def __init__(self,map_fname,player_fname=None):
		ic.configureOutput(outputFunction=logme) 
		self.map_fname = map_fname
		self.save_game_fname = map_fname[:-4]+"sav"
		self.map = MyMap.load_map(map_fname)
		self.roomid = self.map.first_room
		self.room = self.map.get_room(self.roomid)
		self.player_fname=player_fname
		if player_fname is None:
			self.player = Player.generate_player()
		else:
			self.player = Player.load_player(player_fname)
		self.player_menu  = None
		self.game_over = False

	def __repr__(self):
		return f"Name: {self.map.name}; Map file: {self.map_fname}, Player file: {self.player_fname}"	

	def play_game(self):
		ic()
		print(self.player.show_stats())
		print(self.map.first_text)

		while not self.game_over:
			ic()
			self.room.display()
			self.play_turn()

	def play_turn(self):
		self.build_menu()
		self.player_action()

	def player_action(self):
		ic()
		# display menu
		print("\nPlease select your action from the following list by entering the number listed:")
		menu = self.player_menu
		for m_no in menu:
			action = menu[m_no]
			print(f"{m_no}: {action[0]}")

		# get input from player
		pick = int(input("Enter the number: "))
		ic(pick)
		ic(menu[pick][1])

		# execute selection
		desc = menu[pick][0]
		print("\n***\n\nYou chose:",desc)
		if len( menu[pick])<=2 or menu[pick][2] is None:
			menu[pick][1]()
		else:
			ic(menu[pick][2])
			menu[pick][1](menu[pick][2])

	def change_room(self,roomid):
		ic()
		ic(self.room)
		newroom = self.map.get_room(roomid)
		ic(roomid)
		ic(newroom)
		self.room = newroom
		print(self.room.name)
		self.room.display()

	def build_menu(self):
		menu={}
		menu[0]=("View Inventory",self.player.show_inventory)
		menu[1]=("Show Stats",self.player.show_stats)
		menu[2]=("Describe Room",self.room.display)
		midx = 3
		for character in self.room.characters:
			if character.c_type =="Monster":
				monster_name = character.name
				menu[midx] = (f"Attack {monster_name}",self.player.combat_turn,character)
				midx += 1
		ic(self.roomid)
		for door in self.room.doors:
			if door.type != "Secret":
				ic(door.id)
				ic(door.target)
				newroom = door.target
				menu[midx] = (f"Exit through {door.name} door",self.change_room,newroom)
				midx += 1
		for character in self.room.characters:
			if character.c_type=="Friend" or character.c_type=="Neutral":
				menu[midx] = (f"Talk to {character.name},character.id")
		for container in self.room.containers:
			menu[midx]=(f"Open {container.name}",self.open_container,container)
			midx += 1
		menu[midx] = (f"Save Game",self.save_game)
		midx += 1
		menu[midx] = (f"Quit Game",self.quit)
		self.player_menu = menu

	def save_game(self):
		print(f"Saving to {self.save_game_fname}")
		self.map.save_map(self.save_game_fname)
		self.player.save_player(self.save_game_fname,True)

	def quit(self):
		self.game_over=True

	def open_container(self,container):
		for item in container.items:
			self.player.add_item(item)
			container.remove_item(item)

def equip_weapon(player,weapon):
	player.equip_weapon(weapon)

def equip_armor(player,armor):
	player.equip_armor(armor)

def use_magic(room,player,magic):
	mtype = magic.mtype
	if mtype == "Healing":
		pass
	elif mytpe == "Seeing":
		pass
	elif mytpe == "TrapDetect":
		pass
	elif mytpe == "IronSkin":
		pass
	elif mytpe == "Attack":
		pass

	elif mytpe == "Power":
		pass
	elif mytpe == "Accuracy":
		pass
	else:
		ic(f"Bad magic type in play.use_magic {magic}")

if __name__=="__main__":

	print ("Play compiles")
	MAP_FNAME = "Four-foldRiddle.json"
	PLAYER_FNAME = "ReadyPlayer1.json"
	i = Item("I9","Blue Gem","Treasure",1000,"A gem worth 1000 GP.")

	PLAYER_SAVE_FNAME = "current_player.json"
	g = Game(MAP_FNAME,PLAYER_FNAME)
	g.player.add_item(i)
	ic(g)
	print(g)
	
	#g.play_game()

	#ic(len(menu))
	#for idx,desc in menu.items():
	#	print(f"{idx}: 0: {desc[0]}, 1: {desc[1]}")
	"""
	mymap = MyMap.load_map(MAP_FNAME)
	player = Player.load_player(PLAYER_FNAME)
	if player.current_room is None:
		current_room_id = mymap.first_room
		current_room = mymap.rooms[current_room_id]
		player.current_room = current_room

	if player.save_fname is None:
		player.save_name = PLAYER_SAVE_FNAME
	print()
	for item in  player.inventory.items:
		ic(item)
	list_stuff(current_room)
	print()
	"""
	
	print("\nFINISHED WITHOUT ERROR\n")

	

"""
Player 
- can equip weapons in inventory
- can equip armor in inventory
- can use magic in inventory
Room
- can run for door
If there is a monster...
	- can attack monster
else
	- can open containers
if there is non-monster character...
	- can talk to character

Playing the game
- Player turn
	- calculate player's options
	- let player select
	- apply player's option
	- decrement active magic timers
- non-player characters take action
	- monsters in player's room attack & results reported
	- mobile characters move
	- neutral characters react to player's actions
- map actions , if any, take place
"""