import random
from icecream import ic
from mymap import MyMap
from armor import Armor
from weapon import Weapon
from magic import Magic
from item import Item
from container import Container
import json
import os
import time

class Player():
	def backupname(fname):
		name = fname[:-4] # remove extension
		# add a 6 digit number that's roughly seconds from beginning of year
		START = 1700348266.206527
		val = int(time.time()-START)
		name += '{:0>6}'.format(val)
		return "bk/"+name+".bak"

	def export(self):
		newplayer = {"name":self.name,"toughness":self.toughness,"power":self.power,'accuracy':self.accuracy,'hit_points':self.hit_points}
		armors={}
		for atype,armor in self.armor.items():
			if armor is not None:
				armors[atype]=armor.export()
		newplayer['armor']=armors
		inv_dict = self.inventory.export()
		newplayer['inventory']=inv_dict
		newplayer['weapon']=self.weapon.export()
		return newplayer

	def load_player(fname):
		if os.path.isfile(fname):
			with open(fname,"rt") as f:
				data = json.load(f)
			m = Player.ingest(data)
			return m
		else:
			print(f"{fname} does not exist")
			exit()

	def ingest(player_dict):
		new_player = Player(player_dict['name'],player_dict['toughness'],player_dict['power'],player_dict['accuracy'],player_dict['hit_points'])
		for atype,a in player_dict['armor'].items():
			if a is not None:
				newarmor = Armor.ingest(a)
				new_player.equip_armor(newarmor)
		weapon = player_dict['weapon']
		if weapon is not None:
			newweapon = Weapon.ingest(weapon)
			new_player.equip_weapon(newweapon)
		player_dict['inventory'] = Container.ingest(player_dict['inventory'])
		#for i in player_dict['inventory'].:
		#	newitem = Item.ingest(i)
		#	new_player.add_item(newitem)
		return new_player

	def __init__(self,name,toughness,power,accuracy,hp):
		self.name = name
		self.toughness = toughness
		self.power = power
		self.accuracy = accuracy
		self.hit_points = hp
		self.current_room = None
		self.save_fname = None
		self.inventory=Container("inv","Inventory","","Inventory",[])
		self.weapon = None
		self.armor= {}
		self.activemagic = []

		for a in Armor.ARMOR_TYPES:
			self.armor[a]=None

		
	def __repr__(self):
		result = f"Player {self.name}: {self.accuracy}/{self.toughness}/{self.power}\n\tcarrying "   
		if self.weapon is not None:
			result+=f"{self.weapon}\n"
		else:
			result+="no weapon\n" 
		armor_list = "no armor"
		for atype,a in self.armor.items():
			if a is not None:
				if armor_list == "no armor":
					armor_list = f"\tand wearing... \n\t{a}"
				else:
					armor_list+=f" and \n\t{a}"
		result+=armor_list
		return result

	def armor_tough_total(self):
		total = 0
		for t,a in self.armor.items():
			if a is not None:
				ic(a.tough)
				total+=a.tough
		return total

	def show_inventory(self):
		ic()
		print("Inventory:")
		self.inventory.list_items()
		print()

	def show_stats(self): 
		print(f"Your Character {self.name}:")
		print(f"\t Accuracy/Toughness/Power: {self.accuracy}/{self.toughness}/{self.power}\nis carrying...  ")
		result = ""
		if self.weapon is not None:
			result+=f"\t{self.weapon}\n"
		else:
			result+="\tno weapon\n" 
		armor_list = "\tno armor"
		for atype,armor in self.armor.items():
			if armor is not None:
				if armor_list == "\tno armor":
					armor_list = f"\tand wearing... \n\t{armor}"
				else:
					armor_list+=f" and \n\t{armor}"
		result+=armor_list
		print(result)

	def save_player(self,fname=None,append=False):
		opentype = "at"
		if append:
			opentype="wt"
		if fname is None: 
			if self.save_fname is None: 
				self.save_fname = "save_player.json"
			fname = self.save_fname
		if not append and os.path.isfile(fname):
			os.rename(fname, Player.backupname(fname))
		player_dict = self.export()
		data = json.dumps(player_dict,indent=4)
		with open(fname,opentype) as f:
			f.write(data)

	def equip_armor(self,armor):
		atype = armor.atype
		oldarmor = None
		if atype in Armor.ARMOR_TYPES:
			oldarmor = self.armor[atype]
			if oldarmor is not None:
				self.add_item(oldarmor)
			self.armor[atype] = armor
		else:
			print(f"illegal armor type: {atype}")

	def equip_weapon(self,weapon):
		oldweapon = self.weapon
		self.weapon = weapon
		if oldweapon is not None:
			self.add_item(oldweapon)

	def add_item(self,item):
		self.inventory.add_item(item)

	def remove_item(self,item):
		ic(self.inventory)
		ic(len(self.inventory))
		for i in range(0,len(self.inventory)):
			ic(i)
			ic(self.inventory[i])
			if self.inventory[i]==item:
				ic("works",self.inventory[i],item)
				del[self.inventory[i]]
				break
			else:
				ic("didn't work", self.inventory[i],item)
		return item

	def combat_turn(self,monster):
		print(ic(f"Player {self.name} vs Monster {monster.name}"))
		roll = random.randrange(1,20)
		player_to_hit = self.weapon.hit+self.accuracy+roll
		monster_defense = monster.toughness
		print(ic(f"Player to hit: {player_to_hit} ({roll}); monster defense: {monster_defense}"))
		if player_to_hit > monster_defense:
			damage = self.power+self.weapon.damage
			monster.hit_points -= damage
			print(ic(f"Player hits monster & does {damage} damage. Hitpoints:{monster.hit_points}"))
		roll = random.randrange(1,20)
		monster_to_hit = monster.accuracy+roll
		player_defense = self.armor_tough_total() + self.toughness
		print(ic(f"Monster to hit: {monster_to_hit} ({roll}); player defense: {player_defense}"))
		if monster_to_hit > player_defense:
			damage = monster.power
			self.hit_points -= damage
			print(f"Monster hits player & does {damage} damage. Player hitpoints: {self.hit_points}")

 
def generate_player():
	names = ["Fred","Daweed","Daweeded","Lothar"]
	weapon_names = ["Sword","Lance","Bastard","Short Sword","Spear","Club"]
	armors = [("Wood Shield","Shield"),("Horned Helm","Helmet"),("Chainmail","Chest"),("Platearmor","Chest"),("Wrist Bands","Grieves")]
	name = random.choice(names)
	weapon_name = random.choice(weapon_names)
	armor_name = random.choice(armors)
	armor = armors[1]
	tough = 5#random.randrange(1,11)
	power = 5#random.randrange(1,11)
	accuracy = 5#random.randrange(1,11)
	#weapon = 5#random.randrange(1,5)
	damage = 5#random.randrange(1,5)
	power = 5#random.randrange(1,11)
	# = 5#random.randrange(1,5)
	avalue = 20
	hp = 20
	p = Player(name,tough,power,accuracy,hp)
	#print(armor,tough,avalue)#atype,name,tough,value
	armor = Armor("I8","Metal Hat","Just a simple metal hat.",1,"Helmet",5)
	weapon = Weapon("I9","Iron Sword","Simple sword of iron.",5,1,1)
	armor2 = Armor("I7","Hide Shield","A light wooden frame with cow hide.",1,"Shield",2)

	p.equip_armor(armor)
	p.equip_armor(armor2)
	p.equip_weapon(weapon)
	return p

if __name__ == "__main__":
	print("Player compiles!")
	map = MyMap.load_map("Four-foldRiddle.json")
	monster = map.rooms['R1'].characters[0]
	player = Player.load_player("ReadyPlayer1.json")
	ic(player.weapon.hit)
	player.combat_turn(monster)
	#player.save_player("test_player.json")
	#player.save_fname = "test_player2.json"
	#player.save_player()

	#player.show_stats()
	#player.show_inventory()


	print("\nFINISHED WITHOUT ERROR\n")
