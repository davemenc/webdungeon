import json

from icecream import ic

class Door:
	DOOR_TYPES = ["Normal","Locked","BoobyTrapped","Secret","OneWay","Fake"]

	def ingest(door_dict):
		ic(door_dict)
		newdoor = Door(door_dict['id'],door_dict['name'],door_dict['target'],door_dict['type'],door_dict['desc'])
		return newdoor
	def __init__(self,id,name,target,type,desc="Door"):
		self.id = id
		self.name =name
		self.target = target
		self.type = type
		self.desc = desc

	def __repr__ (self):
		return f"Door ({self.id}): {self.name}->{self.target}:  {self.type},{self.desc}"
	def describe(self):
		return(self.desc)

	def export(self):
		return {"id":self.id,"target":self.target,"type":self.type,"name":self.name,"desc":self.desc}

if __name__ == "__main__":
	print ("Door code compiles")
	d = Door("D2","West Passage","R1","Normal","A small door; you'll have to stoop to get through.")

	ic(d)
	exp = d.export()
	ic("EXP",exp)
	ic(json.dumps(exp))
	ic(Door.ingest(exp))
	ic(d.describe())
	print("\nFINISHED WITHOUT ERROR\n")
