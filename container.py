from icecream import ic
import json
from item import Item
from weapon import Weapon
from magic import Magic
from armor import Armor

class Container:
	CONTAINER_TYPES = ["Simple","Locked","Trapped","Breakable","Inventory"]
	CONTAINER_STATES = ["Closed","Open","Destroyed"]
	def ingest(cont_dict):
		container = Container(cont_dict['id'],cont_dict['name'],cont_dict['desc'],cont_dict['type'])
		i=None
		for item in cont_dict['items']:
			itype = item['type']
			if itype == "Weapon":
				i = Weapon(item['id'],item['name'],item['desc'],item['value'],item['hit'],item['damage'])
			elif itype == "Treasure":
				#def __init__(self,id,name,type,desc,value=0):
				i = Item(item['id'],item['name'],itype,item['desc'],item['value'])
			elif itype == "Armor":
				i = Armor(item['id'],item['name'],item['desc'],item['tough'],item['atype'],item['value'])
				#def __init__(self,id,name,desc,tough,atype,value=0):
			elif itype == "Key":
				pass
			elif itype == "Magic":
				#Container.ingest, itype==Magic dict_keys(['name', 'id', 'type', 'desc', 'mtype'])
				{'name': 'Potion Of Seeing', 'id': 'I1', 'type': 'Magic', 'desc': 300, 'mtype': 'Seeing', 'value': 'A clear potion that shimmers.'}
				i = Magic(item['id'],item['name'],item['desc'],item['value'],item['mtype'])
			#def __init__(self,id,name,desc,value,mtype):
				pass
			elif itype == "Map":
				pass
			else:
				print (f"ERROR: bad item type: {itype} in {item}; in function Container.ingest; container: {container}")
			if i is not None:
				container.add_item(i)
		return container

	def __init__(self,id, name, desc,boxtype,items=[]):
		self.id = id
		self.name = name
		self.desc = desc
		self.type = boxtype
		self.items = list()
		self.state = 0
		if isinstance(items,list):
			for item in items:
				if isinstance(item, Item):
					self.items.append(item)
		
	def __repr__(self):
		return f"{self.name}({self.id}: boxtype: {self.type}, contains: {self.items})"

	def list_items(self):
		if len(self.items)==0:
			print("Empty")
		else:
			for item in self.items:
				if item is None:
					ic(self)
					ic("item is none")
				print(item)
		print()

	def add_item(self,item):
		if isinstance(item,Item):
			self.items.append(item)

	def export(self):
		item_list = []
		for item in self.items:
			item_list.append(item.export())
		return {"id":self.id,"name":self.name,"desc":self.desc,"type":self.type,"items":item_list}

	def remove_item(self,item):
		for i in range(0,len(self.items)):
			if self.items[i]==item:
				result = self.items[i]
				del(self.items[i])
				return result

if __name__=="__main__":
	print("Container compiles\n")
	item3 = Item("I3","Red Gem","Treasure","A beautiful red gem.",100)
	item4 = Weapon("I5","Iron Sword","Simple sword.",1,1,1)
	container1=Container("C1", "Gilded Coffer", "A id,name,desc,value,hit,damagewooden box with tiny threads of bright metal running every which way.","Simple",[item3])

	container1.add_item(item4)
	print("list items")
	container1.list_items()
	print("done")
	ic("container1")
	ic(container1)
	exp=container1.export()
	ic(exp)
	ic(json.dumps(exp))
	print("\nFINISHED WITHOUT ERROR\n")
