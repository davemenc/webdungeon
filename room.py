from item import Item
from character import Character
from door import Door
from container import Container
import json
from icecream import ic

class Room:
	def ingest(room_dict):
		print("Room.ingest")
		newroom = Room(room_dict['id'],room_dict['name'],room_dict['desc'])
		print("call container ingest")
		for cont in room_dict['containers']:
			newcont = Container.ingest(cont)
			print("container",newcont)
			newroom.add_container(newcont)
		print("call character ingest")
		for char in room_dict['characters']:
			newchar = Character.ingest(char)
			newroom.add_character(newchar)
		print("call door ingest")
		for door in room_dict['doors']:
			newdoor = Door.ingest(door)
			newroom.add_door(newdoor)
		print("room",newroom)
		print("end room ingest")
		return newroom

	def __init__(self,id,name,description):
		self.id = id
		self.name = name
		self.desc = description
		self.characters=[]
		self.doors = []
		self.containers = []

	def __repr__(self):
		return f"{self.name}({self.id}): {self.desc}...\n\t{self.doors}\n\t{self.characters}\n\t{self.containers} "

	def ingest(room_dict):
		#dict_keys(['id', 'name', 'desc', 'containers', 'characters', 'doors'])
		newroom = Room(room_dict['id'],room_dict['name'],room_dict['desc'])
		for c in room_dict['containers']:
			newcontainer = Container.ingest(c)
			newroom.add_container(newcontainer)
		for ch in room_dict['characters']:
			newchar = Character.ingest(ch)
			newroom.add_character(newchar)
		for d in room_dict['doors']:
			newdoor = Door.ingest(d)
			newroom.add_door(newdoor)
		return newroom

	def describe(self):
		ic()
		result =  f"Room {self.name}: {self.desc}.\n"
		for char in self.characters:
			result += f"There is a {char.describe()} here."
		for door in self.doors:
			result += f"There is a {door.describe()} here."
		ic(result)

		return result

	def display(self):
		print(self.describe())

	def export(self):
		doors=[]
		for door in self.doors:
			d_data = door.export()
			doors.append(d_data)
		chars=[]
		for char in self.characters:
			chars.append(char.export())
		containers = []
		for container in self.containers:
			containers.append(container.export())
		return {'id':self.id,'name':self.name,'desc':self.desc,'containers':containers,'characters':chars,'doors':doors}

	def add_door(self,door):
		self.doors.append(door)

	def add_character(self,character):
		self.characters.append(character)

	def add_container(self,container):
		self.containers.append(container)

if __name__ == "__main__":	
	ic("Room code compiles")
	room2 = Room("R2","Western Triangle", "This triangular room is guarded by a soldier in a formal uniform. You hope he is more used to parading than fighting. There is a cupboard on the opposite wall. There is a door in the SE corner which is called the 'East Door'")
	door2 = Door("D2","West Passage","R2","Normal","A small door; you'll have to stoop to get through.")
	m1 = Character("Ogre",5,5,5,5,"M1")
	#item2 = Armor("I2","Rough Hide Shield",20,1,"Shield")
	item2 = Item("I6","Necklace","Treasure","a delicate chain ",200)
	container1=Container("C1", "Gilded Coffer", "A wooden box with tiny threads of bright metal running every which way.","Simple",[item2])

	room2.add_character(m1)
	room2.add_door(door2)
	room2.add_container(container1)
	room_dict=room2.export()
	ic(room_dict)
	newmap = Room.ingest(room_dict)
	ic(newmap)
	print("\nFINISHED WITHOUT ERROR\n")
	