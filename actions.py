from room import Room
from mymap import MyMap
from player import Player
from item import Item
from character import Character
from door import Door
from container import Container

def list_stuff(room):
	for door in room.doors:
		print(door)
	for character in room.characters:
		print(character)
	for container in room.containers:
		for item in container.items:
			print(item)
if __name__=="__main__":
	print ("Actions compiles")
	print("\nFINISHED WITHOUT ERROR\n")
