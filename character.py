import json
from icecream import ic
import random
from datetime import datetime

class Character:
	CHARACTER_TYPES = ["Monster","Friend","Neutral"]
	def ingest(char_dict):
		newchar = Character(char_dict['name'],char_dict['toughness'],char_dict['power'],char_dict['accuracy'],char_dict['hit_points'],char_dict['id'])
		return newchar

	def __init__(self, name, toughness, power,accuracy,hitpoints,id):
		self.name = name
		self.toughness=toughness
		self.power = power
		self.id = id
		self.accuracy = accuracy
		self.hit_points = hitpoints
		self.c_type = "Monster"
		self.happy = -1 

	def __repr__(self):
		return(f"{self.name} ({self.id}): {self.accuracy}/{self.toughness}/{self.power}/{self.hit_points} ({self.c_type})")

	def describe(self):
		return f"{self.name}"

	def talk(self):
		print("I'm a talkable character.")
	
	def export(self):
		return {'id':self.id,'name':self.name,'toughness':self.toughness, 'power':self.power, 'accuracy':self.accuracy,'hit_points':self.hit_points,'c_type':self.c_type}

	def generate_monster(id):
		names = ["Guard","Soldier","Pirate","Kobold","Ogre","Devil","Crank","Goblin","Witch","Priest","Horror","Pumpkin Head", "Demon","Growler", "Foeman", "Terrorist"]
		name = random.choice(names)
		tough = random.randrange(1,21)
		power = random.randrange(1,21)
		accuracy = random.randrange(1,21)
		hitpoints = random.randrange(1,21)
		return Character(name,tough,power,accuracy,hitpoints,id)

if __name__=="__main__":
	print("character.py compiles!")
	ogre = Character("Ogre",20,20,20,20,"m0")
	ic(ogre)
	wimp = Character("Wimp",1,1,1,1,"m1")
	ic(wimp)
	rand_char = Character.generate_monster("m2")
	ic(rand_char)
	ic(rand_char.export())
	ic(json.dumps(rand_char.export()))
	print("\nFINISHED WITHOUT ERROR\n")

	




