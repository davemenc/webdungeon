from icecream import ic
from item import Item
import json

class Armor(Item):
	ARMOR_TYPES = ["Helmet","Chest","Shield","Grieves"]
	def ingest(armor_dict):
		#ic| armor_dict.keys(): dict_keys(['name', 'id', 'type', 'desc', 'value', 'atype', 'tough'])
		#def __init__(self,id,name,desc,tough,atype,value=0):
		newarmor = Armor(armor_dict['id'],armor_dict['name'],armor_dict['desc'],armor_dict['tough'],armor_dict['atype'],armor_dict['value'])
		return newarmor

	def __init__(self,id,name,desc,tough,atype,value):
		self.tough = tough
		self.atype = atype
		super().__init__(id,name,"Armor",desc,value)


	def __repr__(self):
		return f"{self.name} ({self.id}/{self.type}): desc: {self.desc}, tough: {self.tough}, atype: {self.atype}, value: {self.value}"

	def export(self):
		return {"name":self.name,"id":self.id,"type":self.type,"desc":self.desc,"value":self.value,'atype':self.atype,'tough':self.tough}


if __name__ == "__main__":
	ic ("Armor code compiles")
	

	a = Armor("A3","Leather Shield","A simple cowhide shield.",1,"Shield",2)
	ic(a)

	exp=a.export()
	ic(exp)
	ic(type(exp))
	#ic(json.dumps(exp))
	ic(Armor.ingest(exp))
	print("\nFINISHED WITHOUT ERROR\n")
